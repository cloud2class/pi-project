#!/usr/bin/python
import socket
from struct import *

MaxAcc = 0
AvAcc = 0
RunningTotal = 0
Counter = 0
 
#UDP_IP = "96.49.100.238"
#UDP_IP = "192.168.1.241"
UDP_IP = "0.0.0.0"
#UDP_IP = socket.gethostbyname(socket.gethostname())
print "Receiver IP: ", UDP_IP
UDP_PORT = 5555
#UDP_PORT = int(raw_input ("Enter Port "))
print "Port: ", UDP_PORT   
sock = socket.socket(socket.AF_INET, # Internet
                            socket.SOCK_DGRAM) # UDP
sock.bind((UDP_IP, UDP_PORT))
 
  
print "1,2,3?"
menuoption = input()

while menuoption==1:
	  Counter = Counter + 1
          data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
          #print "received message:", data    
          #print "received message: %1.3f %1.3f %1.3f %1.3f", unpack_from ('!f', data, 0), unpack_from ('!f', data, 4), unpack_from ('!f', data, 8), unpack_from ('!f', data, 12)     
          #print "received message: ", data

	  XAcc = float("%1.4f" %unpack_from ('!f', data, 0))
	  YAcc = float("%1.4f" %unpack_from ('!f', data, 4))
	  ZAcc = float("%1.4f" %unpack_from ('!f', data, 8))

	  XGrav = "%1.4f" %unpack_from ('!f', data, 12)
	  YGrav = "%1.4f" %unpack_from ('!f', data, 16)
	  ZGrav = "%1.4f" %unpack_from ('!f', data, 20)

	  XRot = "%1.4f" %unpack_from ('!f', data, 24)
	  YRot = "%1.4f" %unpack_from ('!f', data, 28)
	  ZRot = "%1.4f" %unpack_from ('!f', data, 32)

	  AOrien = "%1.4f" %unpack_from ('!f', data, 36) #Azimuth
	  POrien = "%1.4f" %unpack_from ('!f', data, 40) #Pitch
	  ROrien = "%1.4f" %unpack_from ('!f', data, 44) #Roll

	  Light = "%1.4f" %unpack_from ('!f', data, 56) #In LUX

	  Proximity = "%1.4f" %unpack_from ('!f', data, 60)

	  if XAcc > MaxAcc:
		MaxAcc = XAcc
	  if YAcc > MaxAcc:
		MaxAcc = YAcc
	  if ZAcc > MaxAcc:
		MaxAcc = ZAcc

	  if XAcc > YAcc and XAcc > ZAcc:
		RunningTotal = RunningTotal + XAcc
	  elif YAcc > ZAcc and YAcc > XAcc:
		RunningTotal = RunningTotal + YAcc
	  else:
		RunningTotal = RunningTotal + ZAcc

	  if Counter == 20:
		AvAcc = RunningTotal / 20
		Counter = 0
		RunningTotal = 0

	  print "Acceleration:  ", XAcc, YAcc, ZAcc
	  print "Gravity:       ", XGrav, YGrav, ZGrav
	  print "Rotation:      ", XRot, YRot, ZRot
	  print "Orientation:   ", AOrien, POrien, ROrien
	  if float(POrien) < -80:
		print "UPRIGHT"
	  elif float(POrien) > -2 and float(POrien) < 2:
		print "LAYING DOWN"
	  print "Ambient Light: ", Light
	  print "Proximity:     ", Proximity
	  print ""
	  print "Maximum Acceleration: ", MaxAcc
	  print "Average Acceleration: ", AvAcc
	  print ""
while menuoption ==2:
	data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
	XAcc = float("%1.4f" %unpack_from ('!f', data, 0))
	YAcc = float("%1.4f" %unpack_from ('!f', data, 4))
	ZAcc = float("%1.4f" %unpack_from ('!f', data, 8))
	HighAcc = 0
	if XAcc > YAcc and XAcc > ZAcc:
		HighAcc=XAcc
	elif YAcc > ZAcc and YAcc > XAcc:
		HighAcc=YAcc
	else:
		HighAcc=ZAcc

	HighAcc = HighAcc*10

	print "#"
	looped = False
	for i in range(0,int(HighAcc)):
		print "#",
		looped = True
	if looped == True:
		print ""
while menuoption ==3:
	data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
	Light = "%1.4f" %unpack_from ('!f', data, 56) #In LUX
	
	for i in range(0,int(float(Light)/10)):
		print "#",
		looped = True
	if looped == True:
		print ""

	  #print "received message: ", "%1.4f" %unpack_from ('!f', data, 0), "%1.4f" %unpack_from ('!f', data, 4), "%1.4f" %unpack_from ('!f', data, 8), "%1.4f" %unpack_from ('!f', data, 12),"%1.4f" %unpack_from ('!f', data, 16), "%1.4f" %unpack_from ('!f', data, 20), "%1.4f" %unpack_from ('!f', data, 24), "%1.4f" %unpack_from ('!f', data, 28),"%1.4f" %unpack_from ('!f', data, 32), "%1.4f" %unpack_from ('!f', data, 36), "%1.4f" %unpack_from ('!f', data, 40), "%1.4f" %unpack_from ('!f', data, 44), "%1.4f" %unpack_from ('!f', data, 48), "%1.4f" %unpack_from ('!f', data, 52), "%1.4f" %unpack_from ('!f', data, 56), "%1.4f" %unpack_from ('!f', data, 60), "%1.4f" %unpack_from ('!f', data, 64), "%1.4f" %unpack_from ('!f', data, 68), "%1.4f" %unpack_from ('!f', data, 72), "%1.4f" %unpack_from ('!f', data, 76), "%1.4f" %unpack_from ('!f', data, 80), "%1.4f" %unpack_from ('!f', data, 84), "%1.4f" %unpack_from ('!f', data, 88), "%1.4f" %unpack_from ('!f', data, 92)
