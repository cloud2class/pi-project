#!/usr/bin/python
import socket,traceback

UDP_IP = "192.168.1.200"
UDP_PORT = 5555

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
sock.bind((UDP_IP, UDP_PORT))
 
while True:
    try:
        data, addr = sock.recvfrom(2048) # buffer size is 1024 bytes
        print "received message:", data
    except (KeyboardInterrupt, SystemExit):
        raise
    except:
        traceback.print_exc()
