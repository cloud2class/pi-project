#!/usr/bin/python

"""
 3D PHONE EMULATOR - Adapted from:
 "Wireframe 3D cube simulation. Developed by Leonel Machava <leonelmachava@gmail.com>"
"""

import sys, math, pygame
#NEW
import socket
from struct import *
#END
class Point3D:
    def __init__(self, x = 0, y = 0, z = 0):
        self.x, self.y, self.z = float(x), float(y), float(z)
 
    def rotateX(self, angle):
        """ Rotates the point around the X axis by the given angle in degrees. """
        rad = angle * math.pi / 180
        cosa = math.cos(rad)
        sina = math.sin(rad)
        y = self.y * cosa - self.z * sina
        z = self.y * sina + self.z * cosa
        return Point3D(self.x, y, z)
 
    def rotateY(self, angle):
        """ Rotates the point around the Y axis by the given angle in degrees. """
        rad = angle * math.pi / 180
        cosa = math.cos(rad)
        sina = math.sin(rad)
        z = self.z * cosa - self.x * sina
        x = self.z * sina + self.x * cosa
        return Point3D(x, self.y, z)
 
    def rotateZ(self, angle):
        """ Rotates the point around the Z axis by the given angle in degrees. """
        rad = angle * math.pi / 180
        cosa = math.cos(rad)
        sina = math.sin(rad)
        x = self.x * cosa - self.y * sina
        y = self.x * sina + self.y * cosa
        return Point3D(x, y, self.z)
 
    def project(self, win_width, win_height, fov, viewer_distance):
        """ Transforms this 3D point to 2D using a perspective projection. """
        factor = fov / (viewer_distance + self.z)
        x = self.x * factor + win_width / 2
        y = -self.y * factor + win_height / 2
        return Point3D(x, y, 1)

class Simulation:
    def __init__(self, win_width = 640, win_height = 480):
        pygame.init()

        self.screen = pygame.display.set_mode((win_width, win_height))
        pygame.display.set_caption("3D PHONE EMULATOR")
        
        self.clock = pygame.time.Clock()

        self.vertices = [
            Point3D(-1.5,0.75,-0.125),
            Point3D(1.5,0.75,-0.125),
            Point3D(1.5,-0.75,-0.125),
            Point3D(-1.5,-0.75,-0.125),
            Point3D(-1.5,0.75,0.125),
            Point3D(1.5,0.75,0.125),
            Point3D(1.5,-0.75,0.125),
            Point3D(-1.5,-0.75,0.125)
        ]

        # Define the vertices that compose each of the 6 faces. These numbers are
        # indices to the vertices list defined above.
        self.faces = [(0,1,2,3),(1,5,6,2),(5,4,7,6),(4,0,3,7),(0,4,5,1),(3,2,6,7)]

        self.angleX, self.angleY, self.angleZ = 0, 0, 0
        
    def run(self):
        """ Main Loop """
	#NEW
	Counter = 0

	UDP_IP = "0.0.0.0"
	print "Receiver IP: ", UDP_IP
	UDP_PORT = 5555
	print "Port: ", UDP_PORT   
	sock = socket.socket(socket.AF_INET, # Internet
                            socket.SOCK_DGRAM) # UDP
	sock.bind((UDP_IP, UDP_PORT))

	AOrien = 0
	POrien = 0
	ROrien = 0

	print "[\033[92m3D Phone Emulator\033[0m]Initiating, please make sure you have the app started and sending data to the correct IP"

	data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
	AOrien = "%1.4f" %unpack_from ('!f', data, 36) #Azimuth
	POrien = "%1.4f" %unpack_from ('!f', data, 40) #Pitch
	ROrien = "%1.4f" %unpack_from ('!f', data, 44) #Roll

	print "[\033[92m3D Phone Emulator\033[0m]To start hold the device landscape with the front facing camera lens nearest the top"

	while float(POrien) > 10 or float(POrien) < -10:
		data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
		AOrien = "%1.4f" %unpack_from ('!f', data, 36) #Azimuth
		POrien = "%1.4f" %unpack_from ('!f', data, 40) #Pitch
		ROrien = "%1.4f" %unpack_from ('!f', data, 44) #Roll

	if float(AOrien) < 0:
		Mode = 1
	else:
		Mode = 2

	#END
        while 1:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit()

            self.clock.tick(50)
            self.screen.fill((0,0,0))

            # Will hold transformed vertices.
            t = []
            
            for v in self.vertices:
                # Rotate the point around X axis, then around Y axis, and finally around Z axis.
                r = v.rotateX(self.angleX).rotateY(self.angleY).rotateZ(self.angleZ)
                # Transform the point from 3D to 2D
                p = r.project(self.screen.get_width(), self.screen.get_height(), 256, 4)
                # Put the point in the list of transformed vertices
                t.append(p)

            for f in self.faces:
                pygame.draw.line(self.screen, (255,255,255), (t[f[0]].x, t[f[0]].y), (t[f[1]].x, t[f[1]].y))
                pygame.draw.line(self.screen, (255,255,255), (t[f[1]].x, t[f[1]].y), (t[f[2]].x, t[f[2]].y))
                pygame.draw.line(self.screen, (255,255,255), (t[f[2]].x, t[f[2]].y), (t[f[3]].x, t[f[3]].y))
                pygame.draw.line(self.screen, (255,255,255), (t[f[3]].x, t[f[3]].y), (t[f[0]].x, t[f[0]].y))
                
            #self.angleX += 1
            #self.angleY += 1
            #self.angleZ += 1

            #NEW CODE START

	    Counter = Counter + 1
	    if Counter == 1:
		Counter = 0

            	data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes

	    	AOrien2 = AOrien #Store previous orientations before taking new reading
	    	POrien2 = POrien
	    	ROrien2 = ROrien

	    	AOrien = "%1.4f" %unpack_from ('!f', data, 36) #Azimuth
	    	POrien = "%1.4f" %unpack_from ('!f', data, 40) #Pitch
	    	ROrien = "%1.4f" %unpack_from ('!f', data, 44) #Roll		
		
		if Mode == 1:
			if float(POrien) < 45 and float(POrien) > -45 and float(AOrien) < 0: #If phone Landscape
				self.angleX += float(ROrien)-float(ROrien2) #Change rotation around X-Axis according to "Roll" change
            			self.angleZ += float(POrien)-float(POrien2) #Change rotation around Z-Axis according to "Pitch" change
			elif float(POrien) < 45 and float(POrien) > -45 and float(AOrien) > 0:
				self.angleX += float(ROrien)-float(ROrien2) #Change rotation around X-Axis according to "Roll" change
            			self.angleZ += float(POrien2)-float(POrien) #Change rotation around Z-Axis according to "Pitch" change
			else: #If phone Portrait (Try to avoid, works best in landscape)
				self.angleZ += (float(AOrien2)-float(AOrien))1.1 #Change rotation around Z-Axis according to "Roll" change
            			#self.angleX += (float(POrien2)-float(POrien)) #Change rotation around X-Axis according to "Pitch" change
		else:
			if float(POrien) < 45 and float(POrien) > -45 and float(AOrien) > 0: #If phone Landscape
				self.angleX += float(ROrien)-float(ROrien2) #Change rotation around X-Axis according to "Roll" change
            			self.angleZ += float(POrien)-float(POrien2) #Change rotation around Z-Axis according to "Pitch" change
			elif float(POrien) < 45 and float(POrien) > -45 and float(AOrien) < 0:
				self.angleX += float(ROrien)-float(ROrien2) #Change rotation around X-Axis according to "Roll" change
            			self.angleZ += float(POrien2)-float(POrien) #Change rotation around Z-Axis according to "Pitch" change
			else: #If phone Portrait (Try to avoid, works best in landscape)
				self.angleZ -= (float(AOrien2)-float(AOrien))/1.1 #Change rotation around Z-Axis according to "Roll" change
            			#self.angleX += (float(POrien2)-float(POrien)) #Change rotation around X-Axis according to "Pitch" change

	    #NEW CODE END

            pygame.display.flip()

if __name__ == "__main__":
    Simulation().run()
