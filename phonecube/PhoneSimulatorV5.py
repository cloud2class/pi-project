#!/usr/bin/python

"""
 3D Phone Emulator
 Version 5.0
 Oliver Martin
"""
import sys, math, pygame
from operator import itemgetter
import socket
from struct import *

""" Global constant defines how often sensor information (Except orientation which is always read) is pulled in. """
DELAY = 20

class Point3D:
    def __init__(self, x = 0, y = 0, z = 0):
        self.x, self.y, self.z = float(x), float(y), float(z)
 
    def rotateX(self, angle):
        """ Rotates the point around the X axis by the given angle in degrees. """
        rad = angle * math.pi / 180
        cosa = math.cos(rad)
        sina = math.sin(rad)
        y = self.y * cosa - self.z * sina
        z = self.y * sina + self.z * cosa
        return Point3D(self.x, y, z)
 
    def rotateY(self, angle):
        """ Rotates the point around the Y axis by the given angle in degrees. """
        rad = angle * math.pi / 180
        cosa = math.cos(rad)
        sina = math.sin(rad)
        z = self.z * cosa - self.x * sina
        x = self.z * sina + self.x * cosa
        return Point3D(x, self.y, z)
 
    def rotateZ(self, angle):
        """ Rotates the point around the Z axis by the given angle in degrees. """
        rad = angle * math.pi / 180
        cosa = math.cos(rad)
        sina = math.sin(rad)
        x = self.x * cosa - self.y * sina
        y = self.x * sina + self.y * cosa
        return Point3D(x, y, self.z)
 
    def project(self, win_width, win_height, fov, viewer_distance):
        """ Transforms this 3D point to 2D using a perspective projection. """
        factor = fov / (viewer_distance + self.z)
        x = self.x * factor + win_width / 2
        y = -self.y * factor + win_height / 2
        return Point3D(x, y, self.z)

class Simulation:
    def __init__(self, win_width = 640, win_height = 480):
        pygame.init()
	
        self.screen = pygame.display.set_mode((win_width, win_height))
        pygame.display.set_caption("Phone Simulator - 5.0")
        
        self.clock = pygame.time.Clock()   

	""" Define shape of 3D object """
        self.vertices = [
            Point3D(-1.5,0.75,-0.125),
            Point3D(1.5,0.75,-0.125),
            Point3D(1.5,-0.75,-0.125),
            Point3D(-1.5,-0.75,-0.125),
            Point3D(-1.5,0.75,0.125),
            Point3D(1.5,0.75,0.125),
            Point3D(1.5,-0.75,0.125),
            Point3D(-1.5,-0.75,0.125)
        ]

        # Define the vertices that compose each of the 6 faces. These numbers are
        # indices to the vertices list defined above.
        self.faces  = [(0,1,2,3),(1,5,6,2),(5,4,7,6),(4,0,3,7),(0,4,5,1),(3,2,6,7)]

        # Define colors for each face
        self.colors = [(255,255,255),(0,153,204),(122,122,122),(0,153,204),(0,153,204),(0,153,204)]
	#                   TOP		SIDE	     BOTTOM	   SIDE	       SIDE	   SIDE
	#		   WHITE 	BLUE	      GREY

        self.angleX = 0
	self.angleY = 0
	self.angleZ = 0
   	
    def run(self):

	font=pygame.font.Font(None,30)

	""" Initialise Variables """
	Counter = 0 
	XAcc = 0
	YAcc = 0
	ZAcc = 0
	Light = 0
	Proximity = 0
	Warned = False

	UDP_IP = "0.0.0.0"
	UDP_PORT = 5555

	sock = socket.socket(socket.AF_INET, # Internet
                            socket.SOCK_DGRAM) # UDP
	sock.bind((UDP_IP, UDP_PORT))
	
	print("[\033[94m\033[1mPhone Simulator\033[0m]Version 5.0") #Current Version

	#Display current IP and PORT to connect to via sensor applicaion on phone.
	print("[\033[94m\033[1mPhone Simulator\033[0m]Initiating, please make sure you have the app started and sending data to the correct IP -",[(s.connect(('8.8.8.8', 80)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1], "Port", UDP_PORT)

	#READ IN DATA PACKETS
	data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes

	#Filter ORIENTATION information from data packets
	AOrien = float("%1.4f" %unpack_from ('!f', data, 36)) #Azimuth
	POrien = float("%1.4f" %unpack_from ('!f', data, 40)) #Pitch
	ROrien = float("%1.4f" %unpack_from ('!f', data, 44)) #Roll
	XGrav = float("%1.4f" %unpack_from ('!f', data, 12))

	print("[\033[94m\033[1mPhone Simulator\033[0m]To start hold the device landscape with the front facing camera lens nearest the top")

	while POrien > 10 or POrien < -10 or (AOrien < 30 and AOrien > -30) or (AOrien > 150 or AOrien < -150) or XGrav > -0.95:
		""" Do not start simulation until phone is landscape and is NOT facing in a direction which would allow the Azimuth orientation to glitch; Due to how it is sensed it increases to 180 and back down again allowing for potential rotation from i.e. 160 to another 160 which would not be sensed as moving at all by the program, and there is no possible way to sense this that I can identify. """
		data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
		AOrien = float("%1.4f" %unpack_from ('!f', data, 36)) #Azimuth
		POrien = float("%1.4f" %unpack_from ('!f', data, 40)) #Pitch
		ROrien = float("%1.4f" %unpack_from ('!f', data, 44)) #Roll
		XGrav = float("%1.4f" %unpack_from ('!f', data, 12))
		if ((AOrien < 30 and AOrien > -30) or (AOrien > 150 or AOrien < -150)) and Warned == False:
			#Warn if they are facing a bad direction for the glitch above and tell them to face a different direction.
			print("[\033[93m\033[1mWARNING\033[0m]Please face in a different direction (Some directions can cause program glitches due to how the phone senses direction)")
			Warned = True #This warning will only be shown once, once the phone is landscape and facing a good direction it will start.

	if AOrien < 0:
		DirectionMode = 1 #Negative Azimuth Start (Affects direction to rotate simulation on screen)
	else:
		DirectionMode = 2 #Positive Azimuth Start (Affects direction to rotate simulation on screen)

	print("[\033[92m\033[1mPhone Simulator\033[0m]Starting...")

	Acctext = font.render("", 1,(255,255,255)) #Initialise text objects to display on screen
        Luxtext = font.render("", 1,(255,255,255)) #Initialise text objects to display on screen
        Proxtext = font.render("", 1,(255,255,255)) #Initialise text objects to display on screen

        """ Main Loop """

        while 1:

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()

            self.clock.tick(50)
            self.screen.fill((0,0,51)) #Background Colour

            # It will hold transformed vertices.
            t = []
            
            for v in self.vertices:
                # Rotate the point around X axis, then around Y axis, and finally around Z axis.
                r = v.rotateX(self.angleX).rotateY(self.angleY).rotateZ(self.angleZ)
                # Transform the point from 3D to 2D
                p = r.project(self.screen.get_width(), self.screen.get_height(), 256, 4)
                # Put the point in the list of transformed vertices
                t.append(p)

            # Calculate the average Z values of each face.
            avg_z = []
            i = 0
            for f in self.faces:
                z = (t[f[0]].z + t[f[1]].z + t[f[2]].z + t[f[3]].z) / 4.0
                avg_z.append([i,z])
                i = i + 1

            # Draw the faces using the Painter's algorithm:
            # Distant faces are drawn before the closer ones.
            for tmp in sorted(avg_z,key=itemgetter(1),reverse=True):
                face_index = tmp[0]
                f = self.faces[face_index]
                pointlist = [(t[f[0]].x, t[f[0]].y), (t[f[1]].x, t[f[1]].y),
                             (t[f[1]].x, t[f[1]].y), (t[f[2]].x, t[f[2]].y),
                             (t[f[2]].x, t[f[2]].y), (t[f[3]].x, t[f[3]].y),
                             (t[f[3]].x, t[f[3]].y), (t[f[0]].x, t[f[0]].y)]
                pygame.draw.polygon(self.screen,self.colors[face_index],pointlist)

	    Counter = Counter + 1 #Increment iteration counter

            data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes

	    AOrien2 = AOrien #Store previous orientations before taking new reading
	    POrien2 = POrien
	    ROrien2 = ROrien
		
	    #Unpack data for orientation
	    AOrien = float("%1.4f" %unpack_from ('!f', data, 36)) #Azimuth
	    POrien = float("%1.4f" %unpack_from ('!f', data, 40)) #Pitch
	    ROrien = float("%1.4f" %unpack_from ('!f', data, 44)) #Roll
		
	    if DirectionMode == 1:
		if POrien < 45 and POrien > -45 and AOrien < 0: #If phone Landscape with camera lens at top
			self.angleX += ROrien-ROrien2 #Change rotation around X-Axis according to "Roll" change
            		self.angleZ += POrien-POrien2 #Change rotation around Z-Axis according to "Pitch" change
		elif POrien < 45 and POrien > -45 and AOrien > 0: #Landscape with camera lens at bottom
			self.angleX += ROrien-ROrien2 #Change rotation around X-Axis according to "Roll" change
            		self.angleZ += POrien2-POrien #Change rotation around Z-Axis according to "Pitch" change
		else: #If phone Portrait (Try to avoid, works best in landscape)
			self.angleZ += (AOrien2-AOrien)/1.5 #Change rotation around Z-Axis according to "Azimuth" change
	    else:
		if POrien < 45 and POrien > -45 and AOrien > 0: #If phone Landscape with camera lens at top
			self.angleX += ROrien-ROrien2 #Change rotation around X-Axis according to "Roll" change
            		self.angleZ += POrien-POrien2 #Change rotation around Z-Axis according to "Pitch" change
		elif POrien < 45 and POrien > -45 and AOrien < 0: #Landscape with camera lens at bottom
			self.angleX += ROrien-ROrien2 #Change rotation around X-Axis according to "Roll" change
            		self.angleZ += POrien2-POrien #Change rotation around Z-Axis according to "Pitch" change
		else: #If phone Portrait (Try to avoid, works best in landscape)
			self.angleZ += (AOrien2-AOrien)/1.5 #Change rotation around Z-Axis according to "Azimuth" chang
		

            if Counter == DELAY:
		""" Every "DELAY" number of iterations read data for other sensor information, this was added to improve performance on Raspberry Pi's which suffered severe lag from reading in all sensor data each loop, this change eliminates any lag """

		XAcc = float("%1.4f" %unpack_from ('!f', data, 0)) #m/s2
	  	YAcc = float("%1.4f" %unpack_from ('!f', data, 4))
	  	ZAcc = float("%1.4f" %unpack_from ('!f', data, 8))

		Light = float("%1.4f" %unpack_from ('!f', data, 56)) #In LUX

		Proximity = float("%1.4f" %unpack_from ('!f', data, 60)) #0 indicates near, 9 indicates far.
		Counter = 0

		if XAcc > YAcc and XAcc > ZAcc:
                	Acc = XAcc #X Acceleration greatest
		elif YAcc > XAcc and YAcc > ZAcc:
			Acc = YAcc #Y Acceleration greatest
		else:
			Acc = ZAcc #Z Acceleration greatest
		
		if Proximity < 9:
			Prox = "Near"
		else:
			Prox = "Far"
			
    		#Render font objects ready to be displayed on screen to show sensor information.
		Acctext=font.render("Acceleration: "+str('%.1f' % abs(Acc)) + " m/s2", 1,(255,255,255))
		Luxtext=font.render("Light: "+str('%.0f' %Light) + " LUX", 1,(255,255,255))
		Proxtext=font.render("Proximity: "+ Prox, 1,(255,255,255))
		
	    #Add all rendered sensor infromation objects to the display
            self.screen.blit(Acctext, (20, 440))
            self.screen.blit(Luxtext, (260, 440)) 
            self.screen.blit(Proxtext, (470, 440)) 
            
            pygame.display.flip() #Update the display

if __name__ == "__main__":
    Simulation().run()
